import App from './App.svelte'
import "./global.css"
import "@google/model-viewer";

const app = new App({
    target: document.body
})

export default app